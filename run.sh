#
# dexfuzzer - Fuzz your dex parsers
# Author: Aseem Jakhar
# Email: aseemjakhar AT gmail DOT com
#
#
# DexFuzzer exec and monitoring script
#

ulimit -c unlimited
dir=$1
echo "dir ($dir)"
oatdir=${dir}_oat
mkdir "$oatdir"

#exit 0
for f in `ls $dir/`
do
  echo "File ($f)"
  dex2oat --dex-file="$dir/$f" --oat-file="$oatdir/$f.oat"
  c=`ls *core* 2> /dev/null`
  if [ "${c}XXX" = "XXX" ]
  then
    echo "No core file foind. Continue"
  else
    echo "Core file found stop. Current dex file was ($f)"
    exit 1
    fi
done
