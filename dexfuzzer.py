#
# dexfuzzer - Fuzz your dex parsers
# Author: Aseem Jakhar
# Email: aseemjakhar AT gmail DOT com
#
#
#!/usr/bin/env python

import argparse
import os.path
import random

from dex import Dex

# --fuzztype Fuzzing techniques
FUZZ_DUMB             = 1
FUZZ_NULL             = 2
FUZZ_NULL_CONSECUTIVE = 3
#FUZZ_MAP_DATA         = 4
FUZZ_TYPE_END         = 4


class DexFuzzer:
    # Constructor
    def __init__(self, args):
        self.dex    = Dex()
        self.args   = args


        if os.path.isfile(self.args.dexfile) != True:
            print "[-] Error: File ({0}) does not exist.".format(args.dexfile)
            print ""
            argp.print_help()
            exit(1)

        self.dex.parse(self.args.dexfile)

    # main program
    def main(self):
        #self.dex.count("\0")
        #exit(0)
        if self.args.hashverify == True:
            print "[?] Verifying hash and checksum of the dex file (%s)" % self.args.dexfile
            self.dex.hashverify()
            exit(0)

        if self.args.outdir is None:
            print "[-] Error: No output directory --outdir provided to store the fuzzed files"
            print ""
            argp.print_help()
            exit(2)

        if os.path.isdir(self.args.outdir):
            print "[-] Error: Output directory ({0}) already exists. Please provide a non-existant name".format(args.outdir)
            print ""
            argp.print_help()
            exit(3)
        else:
            os.mkdir(args.outdir)
        
        if self.args.fuzztype not in range(FUZZ_DUMB, FUZZ_TYPE_END):
            print "[-] Error: Unknown --fuzztype value ({0}) specified. Exiting".format(self.args.fuzztype) 
            exit(4)
        
        print "[?] File to fuzz (%s)" % self.args.dexfile
        print "[?] Percentage of the file/body/null bytes to fuzz (%d)" % self.args.fuzzpercent 
        print "[?] Fuzzing Technique to be used (%d)" % self.args.fuzztype
        print "[?] Total number of fuzzed files to write (%d)" % self.args.numfiles
        print "[?] Output directory for the fuzzed files (%s)" % self.args.outdir
        self.writefuzzfiles()
        print "[+] Wrote the fuzzed files to directory (%s)" % self.args.outdir

    def null_idx_list(self, buf):
        idx = 0
        lst = []
        # get the indexes of all null bytes in buf
        while (idx != -1):
            idx = buf.find("\x00", idx)
            #print "idx found (%d)" % idx
            if idx == -1:
                break
            lst.append(idx)
            idx += 1
        
        #print "Arr Count= (%d)" % len(lst)
        return lst

    #
    # Fuzz null bytes in the buffer.
    #
    def fuzz_null(self, buf):
        arr = list(buf)
        idxlst = self.null_idx_list(buf)
        llen = len(idxlst)
        nbytes = llen * float(self.args.fuzzpercent/100.0)
        #print "Buf len(%d), bytes to write (%d)" % (llen, nbytes)
        for i in xrange(int(nbytes)):
            # Get a random fuzz byte, its ok if it is null, dont bother.
            fbyte = os.urandom(1)
            # Get a random offset within buf (from the null index list)
            foff = idxlst[random.randrange(llen)]
            
            #print "Before: null? byte (%s) at offset(%d)" % (arr[foff].encode("hex"), foff)

            arr[foff] = fbyte

            #print "After: null? byte (%s) at offset(%d) random byte(%s)" % (arr[foff].encode("hex"), foff, fbyte.encode("hex"))

            # Fuzz consecutive null bytes as well if required
            if (self.args.fuzztype == FUZZ_NULL_CONSECUTIVE):
                off = foff + 1
                while off < len(arr) and arr[off] == "\0":
                    #print "before: null consecutive byte (%s) at offset(%d)" % (arr[off].encode("hex"), off)
                    arr[off] = os.urandom(1)
                    #print "after: null consecutive byte (%s) at offset(%d) buflen(%d)" % (arr[off].encode("hex"), off, len(arr))
                    off += 1
                    

        return ''.join(arr)

    def fuzz_dumb(self, buf):
        blen = len(buf)
        arr = list(buf)
        # number of bytes to fuzz
        nbytes = blen * float(self.args.fuzzpercent/100.0)
 
        #print "Buf len(%d), bytes to write (%d)" % (blen, nbytes)
        for i in xrange(int(nbytes)):
            # Get a random fuzz byte
            fbyte = os.urandom(1)
            # Get a random offset within buf
            foff = random.randrange(blen)
            arr[foff] = fbyte
        return ''.join(arr)


    #def fuzz_map_data(self, buf, map_off):
        #blen = len(buf)
        #body = list(buf)

    def writefuzzfiles(self):
        for i in xrange(self.args.numfiles):
            fname = self.args.outdir + "/" + str(i + 1) + ".dex"
            #print "[+]Writing file (%s)" % fname
            self.writefuzzfile(fname)

    #
    # Dumb fuzz the dex file and created crafted dex files
    def writefuzzfile(self, fname):
        fdex = Dex()
        body = ""
        if (self.args.fuzztype == FUZZ_DUMB):
            body = self.fuzz_dumb(self.dex.getbody())
        elif (self.args.fuzztype == FUZZ_NULL or self.args.fuzztype == FUZZ_NULL_CONSECUTIVE):
            body = self.fuzz_null(self.dex.getbody())
        #elif (self.args.fuzztype == FUZZ_MAP_DATA):
            #self.dex.print_dex()
            #exit(0)
        else:
            print "[-] Invalid fuzz type (%d) specified" % self.args.fuzztype
            return

        fdex.construct(self.dex, body)
        fdex.writefile(fname)


#########################################################################################
#                                   SCRIPT START                                        #
#########################################################################################

print "\nDexfuzzer 1.0\n"
print "It's Fuzzing Dexy\n"

argp = argparse.ArgumentParser(description='DexFuzzer - Dex File Fuzzer')
argp.add_argument("dexfile", metavar="file.dex", help="Full path of the dex file to fuzz")
argp.add_argument("-t", "--fuzztype",
                  default='1',
                  type=int,
                  help="Fuzzing technique to be used. 1 = dumb fuzz dex body, 2 = Fuzz null bytes in body randomly. 3 = Same as 2 but also fuzz any consecutive null bytes, if any, immediately after each fuzz location. Default is 1")

argp.add_argument("-p", "--fuzzpercent",
                  default='10',
                  type=int,
                  help="Percentage of file/body/null etc to fuzz. Default is 10")
argp.add_argument("-n", "--numfiles",
                  default='1',
                  type=int,
                  help="Number of fuzzed dex files to create. They are named as numbers and saved in --outdir directory. Default is 1")
argp.add_argument("-s", "--hashverify",
                  action='store_true',
                  help="Verify the adler32 checksum and SHA1 hash of the dex file")
argp.add_argument("-o", "--outdir",
                  help="Full path of new output directory name for the fuzzed files. It should be non-exist")

args = argp.parse_args()

df = DexFuzzer(args)
df.main()

# The unpack returns a number whereas encode returns the byte value as hex representation (i.e. "\xab" to "ab")
# encode = "\xab" => "ab"
# decode = "ab" => "\xab"
# struct.unpack("I", bytestring) = "\xab" => 0xab


