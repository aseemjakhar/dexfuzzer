#
# dexfuzzer - Fuzz your dex parsers
# Author: Aseem Jakhar
# Email: aseemjakhar AT gmail DOT com
#
from zlib import adler32
from hashlib import sha1
import struct

# Size Constants for Dex
UINTSZ = 4
MAGICSZ = 8
SHA1SZ = 20 

class Dex:

#  def __init__(self):
#    self.fname

    def parse(self, fname):
        self.origfile = fname
 
        fo = open(self.origfile)

        # Read the Meta data
        # Format explained in https://source.android.com/devices/tech/dalvik/dex-format.html
        self.magic = fo.read(MAGICSZ)

        # Checksum is adler32
        self.checksum = fo.read(UINTSZ)

        # Signature is SHA1
        self.signature = fo.read(SHA1SZ)

        self.file_size = fo.read(UINTSZ)

        # Header size is 0x70 (112 decimal) bytes as of 17 July 2014
        self.header_size = fo.read(UINTSZ)

        self.endian_tag = fo.read(UINTSZ)
        self.link_size = fo.read(UINTSZ)
        self.link_off = fo.read(UINTSZ)
        self.map_off = fo.read(UINTSZ)
        self.string_ids_size = fo.read(UINTSZ)
        self.string_ids_off = fo.read(UINTSZ)
        self.type_ids_size = fo.read(UINTSZ)
        self.type_ids_off = fo.read(UINTSZ)
        self.proto_ids_size = fo.read(UINTSZ)
        self.proto_ids_off = fo.read(UINTSZ)
        self.field_ids_size = fo.read(UINTSZ)
        self.field_ids_off = fo.read(UINTSZ)
        self.method_ids_size = fo.read(UINTSZ)
        self.method_ids_off = fo.read(UINTSZ)
        self.class_defs_size = fo.read(UINTSZ)
        self.class_defs_off = fo.read(UINTSZ)
        self.data_size = fo.read(UINTSZ)
        self.data_off = fo.read(UINTSZ)
        self.body = fo.read()


        #Finally close the file
        fo.close()
        #moff = self.get_body_map_offset()
        #msize = struct.unpack("<I", self.body[moff])
        

    #
    #  Return the offset of map from the start of dex body
    #
    #def get_body_map_offset(self):
        #return ((struct.unpack("I", self.map_off)[0]) - (struct.unpack("I", self.header_size)[0]))


    #
    # Print dex header and other info. Used mainly for debugging
    #
    def print_dex(self):
        #print "[*] Magic            (%d)" % struct.unpack("8B", self.magic)
        print "[*] Magic            (%s)" % self.magic
        print "[*] Checksum         (%x)" % struct.unpack("<I", self.checksum)
        print "[*] Signature        (%s)" % self.signature.encode("hex")
        print "[*] file_size        (%d)" % struct.unpack("I", self.file_size)
        print "[*] header_size      (%d)" % struct.unpack("I", self.header_size)
        print "[*] endian_tag       (%d)" % struct.unpack("I", self.endian_tag)
        print "[*] link_size        (%d)" % struct.unpack("I", self.link_size)
        print "[*] link_off         (%d)" % struct.unpack("I", self.link_off)
        print "[*] map_off          (%d)" % struct.unpack("I", self.map_off)
        print "[*] string_ids_size  (%d)" % struct.unpack("I", self.string_ids_size)
        print "[*] string_ids_off   (%d)" % struct.unpack("I", self.string_ids_off)
        print "[*] type_ids_size    (%d)" % struct.unpack("I", self.type_ids_size)
        print "[*] type_ids_off     (%d)" % struct.unpack("I", self.type_ids_off)
        print "[*] proto_ids_size   (%d)" % struct.unpack("I", self.proto_ids_size)
        print "[*] proto_ids_off    (%d)" % struct.unpack("I", self.proto_ids_off)
        print "[*] field_ids_size   (%d)" % struct.unpack("I", self.field_ids_size)
        print "[*] field_ids_off    (%d)" % struct.unpack("I", self.field_ids_off)
        print "[*] method_ids_size  (%d)" % struct.unpack("I", self.method_ids_size)
        print "[*] method_ids_off   (%d)" % struct.unpack("I", self.method_ids_off)
        print "[*] class_defs_size  (%d)" % struct.unpack("I", self.class_defs_size)
        print "[*] class_defs_off   (%d)" % struct.unpack("I", self.class_defs_off)
        print "[*] data_size        (%d)" % struct.unpack("I", self.data_size)
        print "[*] data_off         (%d)" % struct.unpack("I", self.data_off)

        print ""
        print "[*] Dex in-memory body size (%d)" % len(self.body)
        print "[*] map offset in body (%d)" % ((struct.unpack("<I", self.map_off)[0]) - (struct.unpack("<I", self.header_size)[0])) 
    #
    # Returns the body of the dex file i.e. starting right after the header
    #
    def getbody(self):
        return self.body

    #
    # Sets the body in the dex file i.e. starting right after the header
    #
    def setbody(self, body):
        self.body = body

    #
    # Construct a dex from a given dex object and the body. 
    # Recalculate and set the new SHA1 and adler32 checksum
    #
    # @param  dex   The dex object to copy the headers from
    # @param  body  The body portion of the dex
    #
    def construct(self, dex, body):
        self.copyheaders(dex)
        self.setbody(body)
        self.rehash()


    def rehash(self):
        #
        # In the headers adler32 comes first followed by sha1 signature, so calculate sha1 first
        # and then adler32 as its checksum calculation also includes the sha1 header value 
        self.signature = self.calcsha1()
        self.checksum  = self.calcadler32()

    #
    # Write the dex back to a file
    #
    # @param file   The file to write the dex content to
    #
    def writefile(self, file):
        fo = open(file, 'w')
        fo.write(self.magic + self.checksum + self.signature + self.file_size + self.header_size +
                  self.endian_tag + self.link_size + self.link_off + self.map_off + self.string_ids_size +
                  self.string_ids_off + self.type_ids_size + self.type_ids_off + self.proto_ids_size + 
                  self.proto_ids_off + self.field_ids_size + self.field_ids_off + self.method_ids_size +
                  self.method_ids_off + self.class_defs_size + self.class_defs_off + self.data_size + 
                  self.data_off + self.body)
        fo.close()


    #
    # Copy the dex headers from a given dex object 
    # Note: sha1 and adler32 are blindly copied, assuming they will be recalculated before saving the
    # crafted file.
    #
    # @param  dex   The dex object to copy the headers from
    #
    def copyheaders(self, dex):
        self.origfile = dex.origfile
        self.magic = dex.magic
        # Although this will be recalculated for each fuzzed output but just copied for uniformity
        self.checksum = dex.checksum
        # Although this will be recalculated for each fuzzed output but just copied for uniformity
        self.signature = dex.signature
        self.file_size = dex.file_size
        self.header_size = dex.header_size
        self.endian_tag = dex.endian_tag
        self.link_size = dex.link_size
        self.link_off = dex.link_off
        self.map_off = dex.map_off
        self.string_ids_size = dex.string_ids_size
        self.string_ids_off = dex.string_ids_off
        self.type_ids_size = dex.type_ids_size
        self.type_ids_off = dex.type_ids_off
        self.proto_ids_size = dex.proto_ids_size
        self.proto_ids_off = dex.proto_ids_off
        self.field_ids_size = dex.field_ids_size
        self.field_ids_off = dex.field_ids_off
        self.method_ids_size = dex.method_ids_size
        self.method_ids_off = dex.method_ids_off
        self.class_defs_size = dex.class_defs_size
        self.class_defs_off = dex.class_defs_off
        self.data_size = dex.data_size
        self.data_off = dex.data_off



    #
    # Calculates the adler32 checksum of the in-memory dex content as per the specification
    #
    # @return  The 4 byte string containing the adler32 checksum value   
    def calcadler32(self):

        csum = adler32(self.signature + self.file_size + self.header_size + self.endian_tag + self.link_size +
                       self.link_off + self.map_off + self.string_ids_size + self.string_ids_off +
                       self.type_ids_size + self.type_ids_off + self.proto_ids_size + self.proto_ids_off + 
                       self.field_ids_size + self.field_ids_off + self.method_ids_size + self.method_ids_off +
                       self.class_defs_size + self.class_defs_off + self.data_size + self.data_off + self.body)

        # Convert it to unsigned int
        if (csum < 0):
            print "[*] Checksum is negative (%x)" % csum
            csum += 2 ** 32
            print "[*] Checksum converted to Unsigned (%x)" % csum

        #print "Actual   ADLER32 [%s]" % self.checksum.encode("hex")
        #print "Computed ADLER32 [%s]" % hex(csum)
        #print "Actual   ADLER32 str [%s]" % self.checksum
        #print "Computed ADLER32 str [%s]" % struct.pack("<I", csum)
        #if self.checksum == struct.pack("<I", csum):
        #    print "Yippie adler32 equal" 
        return struct.pack("<I", csum)


    #
    # Calculates the SHA1 signature of the in-memory dex content as per the specification
    #
    # @return  The 20 byte string containing the SHA1 value
    def calcsha1(self):

        dsha1 = sha1()
        dsha1.update(self.file_size + self.header_size + self.endian_tag + self.link_size +
                     self.link_off + self.map_off + self.string_ids_size + self.string_ids_off +
                     self.type_ids_size + self.type_ids_off + self.proto_ids_size + self.proto_ids_off + 
                     self.field_ids_size + self.field_ids_off + self.method_ids_size + self.method_ids_off +
                     self.class_defs_size + self.class_defs_off + self.data_size + self.data_off + self.body)
    
        #print "Actual   SHA1 [%s]" % self.signature.encode("hex")
        #print "Computed SHA1 [%s]" % dsha1.hexdigest()
        #print "Actual   SHA1 str [%s]" % self.signature
        #print "Computed SHA1 str [%s]" % dsha1.digest()

        return dsha1.digest() 

    #
    # Compute the adler32 checksum and sha1 of the dex file and match them with the
    # respective values present in the dex header
    #
    def hashverify(self):
        csha1 = self.calcsha1()
        cadler32 = self.calcadler32()

        print "[*] Actual   ADLER32 [%s]" % self.checksum.encode("hex")
        print "[*] Computed ADLER32 [%s]" % cadler32.encode("hex")
        print "[*] Actual   ADLER32 str [%s]" % self.checksum
        print "[*] Computed ADLER32 str [%s]" % cadler32

        print "[*] Actual   SHA1 [%s]" % self.signature.encode("hex")
        print "[*] Computed SHA1 [%s]" % csha1.encode("hex")
        print "[*] Actual   SHA1 str [%s]" % self.signature
        print "[*] Computed SHA1 str [%s]" % csha1

        if self.signature != csha1:
            print "[-] Actual SHA1 in the dex header does not match the computed SHA1"
        else:
            print "[+] Actual SHA1 in the dex header matches the computed SHA1"

        if self.checksum != cadler32:
            print "[-] Actual ADLER32 in the dex header does not match the computed ADLER32"
        else:
            print "[+] Actual ADLER32 in the dex header matches the computed ADLER32"


    #
    # for testing/debugging only
    #
    # To count null bytes in a file ($od -cx file.dex | grep -on "\\\0" | wc -l)
    def count(self, subs):
        dstr = self.magic + self.checksum + self.signature + self.file_size + self.header_size +       \
              self.endian_tag + self.link_size + self.link_off + self.map_off + self.string_ids_size + \
              self.string_ids_off + self.type_ids_size + self.type_ids_off + self.proto_ids_size +     \
              self.proto_ids_off + self.field_ids_size + self.field_ids_off + self.method_ids_size +   \
              self.method_ids_off + self.class_defs_size + self.class_defs_off + self.data_size +      \
              self.data_off + self.body

        print "[*] Count of substring (in hex=(%s)) in dex is (%d) and in dex body is (%d)" \
              % (subs.encode("hex"), dstr.count(subs), self.body.count(subs))

